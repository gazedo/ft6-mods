# FT6-Mods

Modifiable files for the ft6 to enable people to keep upgrading and making this monster 3d printer awesome!

# Repo Guidelines
For pull requests:
- Naming Convention - ```${PartIdentifier}_${Side(A for rear and B for front)}_${specific_application}_${revision}```
  - IE X-Axis_Mount_B_r1.0.step
    - Means this is the mount to attach the x axis and this part fits on the front of the printer.
  - IE ExtruderMount_rj45_r1.0.step
    - Would mean this extruder mount has a spot for a rj45 connector mount.
- If the pull request is a modification to the file that maintains general use without adding any special applications, replace the current file. If the modification adds an application, IE adds a rj45 mount, then duplicate the file and merge a copy.


# Version Differences
Version r2.0 is meant for compatibility with the Bondtech LGX and a Mosquitto hotend while also moving the bltouch to be height adjustable. The fanshroud is not the best but is sufficient for my use. If someone wants a better one, a pull request would be appreciated.
