# Conversion kit for the Folgertech FT6 to convert to CoreXY

All parts are included in step file format. For the idler mount, you need to mirror the file in whatever slicer you're using.

# Required parts

These conversion parts are designed to work with:
- Right hand Bondtech BMG
- E3D style and height hotend
  - Compatible with the Triangle Labs Dragon
- 6x toothed pulleys
- 2x smooth pulleys
- ~5.5m of gt2 6mm belt
- m5 hardware for mounting pulleys and mounts to the extrusion
- 4x m3 button head screws ~10mm - 16mm to mount the carriage to the slider
